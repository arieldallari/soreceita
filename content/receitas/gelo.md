---
title: "Gelo"
date: 2021-10-15T18:31:41-03:00
draft: false
image: /images/gelo.jpg
dificuldade: "1"
tempo: "30 min"
---

#### Tempo de preparo: 30 min


## Ingredientes
- Água fitrada
- Forma de gelo

## Modo de Preparo

Colocar a água filtrada na forma de gelo e colocar a forma no freezer. Após 30 minutos, retirar a forma da geladeira e separar os gelos individuais da forma