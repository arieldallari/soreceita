---
title: "Pipoca"
date: 2021-10-14T20:58:46-03:00
draft: false
image: /images/pipoca.jpg
dificuldade: "2"
tempo: "10 min"
---



#### Tempo de preparo: 10 min

## Ingredientes
- 2/3 de xícara (chá) de milho para pipoca
- 3 colheres (sopa) de óleo
- Sal a gosto

## Modo de Preparo
Acrescentar o óleo em uma panela fechada a fogo médio, junto com 3 grãos de milho. Quando os grãos estourarem, adicionar o resto do milho e mexer a panela constantemente, deixando a tampa da panela levemente aberta. Quando o intervalo entre estouros de grão for superior a 2 segundos, apagar o fogo, servir e adicionar o sal.