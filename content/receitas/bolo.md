---
title: "Bolo"
date: 2021-10-14T21:34:58-03:00
draft: false
image: /images/bolo.jpg
dificuldade: "4"
tempo: "1 hora"
---



#### Tempo de preparo: 1h

## Ingredientes
- 1 pacote de bolo
- 3 ovos
- 100 mL de leite integral
- 50 g de margarina

## Modo de Preparo

Com um liquidificador, misturar o pacote de bolo com os ovos, leite e margarina. Despejar a mistura em uma assadeira untada com manteiga. Colocar por aproximadamente 1h no forno médio. Caso um palito espetado no bolo saia limpo, retirar o bolo do forno e deixar esfriar.

