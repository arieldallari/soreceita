---
title: "Ovo Frito"
date: 2021-10-15T18:31:56-03:00
draft: false
image: /images/ovofrito.jpg
dificuldade: "2"
tempo: "5 min"
---

#### Tempo de preparo: 5 min


## Ingredientes
- Ovo
- Azeite
- Sal a gosto

## Modo de Preparo

Em uma panela antiaderente, colocar azeite no fogo médio. Após o óleo esquentar, colocar o ovo e o sal. Após 3 minutos, virar o ovo com uma espátula. Após mais 2 minutos, apagar o fogo e retirar o ovo.