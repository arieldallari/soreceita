---
title: "Falafel"
date: 2021-10-14T20:36:51-03:00
draft: false
image: /images/falafel.jpg
dificuldade: "5"
tempo: "1 hora"
---



#### Tempo de preparo: 1h


## Ingredientes
- 1 pacote de grão de bico (500g)
- 1 cebola média
- 4 dentes de alho
- Cebolinha
- 1 colher de chá de cominho
- Sal e pimenta do reino a gosto
- Óleo para fritura

## Modo de Preparo

No dia anterior, colocar o grão de bico em água, para que possa crescer. Deve-se então secar o grão de bico e, com um processador, triturar e misturar todos os ingredientes.Quando obter uma massa uniforme, resfriar na geladeira enquanto o óleo para fritar esquenta.

Com o óleo quente, montar bolinhas com a massa e colocar cuidadosamente no óleo, retirando quando a massa estiver com uma cor marrom-dourada. Deixar em um papel toalha para o óleo remanescente escorrer e em seguida está pronto para ser servido!
